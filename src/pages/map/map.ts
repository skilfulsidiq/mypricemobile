import { Component,ViewChild ,ElementRef  } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { Geolocation ,GeolocationOptions ,Geoposition } from '@ionic-native/geolocation';
// import { GoogleMaps, GoogleMap, GoogleMapsEvent, LatLng, CameraPosition, MarkerOptions, Marker } from '@ionic-native/google-maps';
import {LaunchNavigator,LaunchNavigatorOptions} from "@ionic-native/launch-navigator";
// import { Diagnostic } from '@ionic-native/diagnostic';


declare var google:any;
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

  options : GeolocationOptions;
  currentPos:Geoposition;
  start:any;
  end;
  latitude:number ;
  longitude:number ;


  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  map: any;
  // map: GoogleMap;
  constructor(public navCtrl: NavController,private geolocation : Geolocation,
              public navParam:NavParams,
              public navigator:LaunchNavigator,public platform:Platform) {
    this.end = this.navParam.get("address");
    console.log(this.end);



  }

  ionViewDidLoad() {
    // this.initMap();
   
      this.getLocation();
    

    console.log('ionViewDidLoad SearchPage');
  }
  // showAlert(text) {

  //   let alert = this.alertCtrl.create({
  //     title: 'Info',
  //     subTitle: text,
  //     buttons: [{
  //       text: 'Ok',
  //       role: 'ok',
  //       handler: () => {
  //         this.diagnostic.switchToLocationSettings();
  //         this.getLocation();
  //         console.log('Ok clicked');
  //       }
  //     }]
  //   });
  //   alert.present();
  // }
  // initMap(){
  //   let element = this.mapElement.nativeElement();
  //   this.map = this.googlemaps.create(element.nativeElement);
  //
  // }
  // locateUser(){
  //   let options = {
  //     enableHighAccuracy: true,
  //     timeout: 25000
  //   };
  //   this.geolocation.getCurrentPosition(options).then(position=>{
  //     let loc = new LatLng(position.coords.latitude,position.coords.longitude);
  //     this.start = {
  //       latitude: position.coords.latitude,
  //       longitude: position.coords.longitude
  //     };
  //     // this.moveCamera(loc);
  //
  //   },err=>{console.log(err)});
  // }
  //
  // moveCamera(loc:LatLng) {
  //   let option: CameraPosition<LatLng> = {
  //     target: loc,
  //     zoom: 15,
  //     tilt: 10,
  //     bearing:50
  //   }
  //   this.map.moveCamera(option);
  // }
  // createMarker(loc:LatLng,title:string) {
  //   let markerOptions: MarkerOptions = {
  //     position: loc,
  //     title: title
  //   }
  //   return this.map.addMarker(markerOptions);
  // }

  getLocation(){
    
    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then(position=>{
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      this.start = latLng;
      let mapOptions = {
        center: latLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.startNavigating(latLng,this.end);
      // let watch = this.geolocation.watchPosition()
      // .filter((p) => p.coords !== undefined) //Filter Out Errors
      // .subscribe(position => {
      //     console.log(position.coords.longitude + ' ' + position.coords.latitude);
      //     });

      
    });
  }

  startNavigating(start,end){
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;


    directionsDisplay.setMap(this.map);
    directionsDisplay.setPanel(this.directionsPanel.nativeElement);
    directionsService.route({
      origin: start,
      destination:end,
      travelMode: google.maps.TravelMode['DRIVING']
    }, (res, status) => {

      if(status == google.maps.DirectionsStatus.OK){
        directionsDisplay.setDirections(res);

      } else {
        console.warn(status);
      }

    });

  }



  // navigateLocation(lat,long, end){
  //   let options: LaunchNavigatorOptions = {
  //     start:[lat,long],
  //     app: this.navigator.APP.UBER
  //   };
  //   this.navigator.navigate(end+', ON', options)
  //   .then(success =>{
  //     console.log(success);
  //   },error=>{
  //     console.log(error);
  //   })
  // }
}
