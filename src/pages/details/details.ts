import { Component } from '@angular/core';
import { BrowserTab } from '@ionic-native/browser-tab';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {
  AlertController,
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  NavParams, Toast,
  ToastController
} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {SqliteProvider} from "../../providers/sqlite/sqlite";
import {ChecklistProvider} from "../../providers/checklist/checklist";
import {RecentviewProvider} from "../../providers/recentview/recentview";


@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  buttonIcon: string = "heart-outline";
product;
user;
loading:Loading;
storeprice:any;
toast:Toast;
lists = [];
dealprice:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public loadingCtrl:LoadingController,public  auth:AuthProvider,
              public browser:BrowserTab,public toastCtrl:ToastController,
              public inapp:InAppBrowser,public sqliteService:SqliteProvider,public recent:RecentviewProvider,
              public checklist:ChecklistProvider,public alertCtrl:AlertController) {
        //      open db
          this.recent.openDb();

        if (this.navParams.get("dealprice")){
              this.dealprice = true;
        }
      this.product = this.navParams.get("oneproduct");
        this.addToReview(this.product);
    this.user = this.navParams.get("user");
      if(this.navParams.get("storeprice") != undefined || this.navParams.get("storeprice") != null ){
        this.storeprice = this.navParams.get("storeprice");
      }else{
        this.getRelatedPrice(this.product[0].id);
      }

    console.log("productid"+this.product[0].id);
    this.sqliteService.openDb();
      this.getData();

  }

  ionViewDidLoad() {
    if(this.lists == null){
      this.getData();
    }
    console.log("this store"+this.storeprice)

    console.log('ionViewDidLoad DetailsPage');
  }
  ionViewWillEnter() {
    if(this.lists == null){
      this.getData();
    }

  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }

  locatestore(id){
    console.log("locateid"+ id);

    this.navCtrl.push("MaplistPage",{id:id});
  }
  goback(){

    this.navCtrl.setRoot("SearchPage");
  }
  async goUrl(url:string){
    this.showLoading();
    try {
        const isAvailable = await this.browser.isAvailable();
        console.log(isAvailable);
        if(isAvailable){
          this.dismissLoading();
            await this.browser.openUrl(url);
        }else{

          this.inapp.create(url,'_blank');
        }
    } catch (error) {
      console.log(error);
      // new window.URL(url);

    }
  }

  getRelatedPrice(id){
    this.auth.loadThreeStorePrice(id).subscribe(data=>{
      this.storeprice = data;
      console.log(data)
    },error1 => {
      console.log();
    });
  }

  getData() {
    this
      .sqliteService
      .getRows()
      .then(s => {
        this.lists = this.sqliteService.arr;
      }).catch(err=>{
      console.log(err)
    });
  }

  showAddToList(one){
    console.log(this.lists);
    var id;
    let alert = this.alertCtrl.create();
    alert.setTitle('Add Item');
    for (let i = 0; i < this.lists.length; i++ ) {
      id=this.lists[i]['id'];
      alert.addInput({
        name:'listid',
        type: 'radio',
        label: this.lists[i]['title'],
        value: id,
        checked: false
      });

    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Add to List',
      handler: data => {
        console.log(data.listid);
        console.log(id);

        this.processList(id,one.name,one.price);
      }
    });
    alert.present();
  }

  processList(product,store,price){
    this.sqliteService.addItem(product, store, price).then(res=>{
      console.log( product,store, price);
      if(res){
        this.presentToast(product+" added to List");
      }else{
        this.presentToast("Error, try again");
      }
    }).catch(e=>{console.log(e)});
  }
  //add to shoping
  addToShopping(product){
    console.log(product.name);
    console.log(product.shopowner);
    console.log(product.price);
    let alert = this.alertCtrl.create({
      title: 'Shopping List',
      message: "Do you want to add "+product.name + " to your shopping List",
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.processList(product.name, product.shopowner, parseFloat(product.price));
            console.log(parseFloat(product.price));
          }
        }
      ]
    });

    //  = this.alertCtrl.create();
    // alert.message();
    //
    // alert.addButton('Cancel');
    // alert.addButton({
    //   text: 'Add to List',
    //   handler: data => {
    //     console.log(data);
    //     console.log();
    //
    //
    //   }
    // });
    alert.present();
  }

  addToReview(productview){
    let product = productview[0];
      console.log(product[0]);
    let name = product.name;
    let id = product.id;
    let price = product.price;
    let image = product.productimage;
    let store = product.shopowner;
    this.recent.addRecentView(name,image,price,store,id).then(res=>{
      if(res){

      }
    }).catch(err=>{console.log(err)})
  }


}
