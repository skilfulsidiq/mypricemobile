import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams, Toast, ToastController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {FavouriteProvider} from "../../providers/favourite/favourite";

/**
 * Generated class for the HomesearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homesearch',
  templateUrl: 'homesearch.html',
})
export class HomesearchPage {
    searchProduct:any =[];
    value:string;
    isTrue:boolean = true;
    loading:Loading;
    storeprice;
    allproduct:any = [];
    toast:Toast;
  initialButtonIcon = "ios-heart-outline";
  constructor(public navCtrl: NavController, public navParams: NavParams,public favour:FavouriteProvider,public toastCtrl:ToastController,
              public auth:AuthProvider,public loadingCtrl:LoadingController) {
    this.value = this.navParams.get('value');
    console.log(this.value);
    this.searchResult(this.value);
    this.loadProduct();

    // console.log(this.searchProduct.length);
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HomesearchPage');
  }

  searchResult(value){
      this.showLoading();
      this.auth.searchProduct(value).subscribe(res=>{
        this.dismissLoading();
        this.searchProduct = res;
        console.log("all product from homesearch page 1 "+ this.searchProduct);
        if (this.searchProduct === undefined || this.searchProduct.length == 0) {
          // array empty or does not exist
          this.isTrue = false;

        }else{
          this.isTrue = true;
        }
        // console.log(this.searchProduct);
      },err=>{
        console.log(err);
      })
  }

  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      // this.loading.setDuration(1000)
      this.loading = null;
    }

  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }
  loadProduct(){
    // this.showLoading();
    this.auth.loadProduct().subscribe(res=>{
      // this.dismissLoading();
      this.allproduct = res;
      console.log("all product from homesearch page "+ this.allproduct);
    },err=>{console.log(err)})
  }
  goDetail(id) {
    console.log(id);
    this.showLoading();
    this.auth.loadAProduct(id).subscribe(data => {
      this.auth.loadThreeStorePrice(id).subscribe(data2=>{
        this.dismissLoading();
        this.storeprice = data2;
        console.log(data)
      },error1 => {
        console.log();
      });

      this.navCtrl.setRoot("DetailsPage", {oneproduct: data,storeprice:this.storeprice});
    }, error => {
      console.log(error);
    })

  }
  addToFavour(name,image,price,store,id,product){
    this.favour.addFavourite(name,image,price,store,id).then(res=>{
      if(res){

        if(product.icon == "heart"){
          this.favour.del(id).then(yes=>{
            if(yes){
              product.icon = "ios-heart-outline";
              this.presentToast("remove from favourite");
            }
          },err=>{console.log(err)});
        }else{
          product.icon = "heart";
          this.presentToast("added to Favourite");
        }

      }
    }).catch(err=>{console.log(err)})
  }
}
