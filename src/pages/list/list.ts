import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  LoadingController,
  Loading,
  AlertController
} from 'ionic-angular';

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {SqliteProvider} from "../../providers/sqlite/sqlite";
import {ChecklistProvider} from "../../providers/checklist/checklist";
/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
  loading:Loading;
check:boolean = false;
lists = [];
sum = 0;
qty:number;
amount:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl:LoadingController,public alertCtrl:AlertController,
              public transition:NativePageTransitions, public sqliteService:SqliteProvider,public checklist:ChecklistProvider) {
    this.sqliteService.openDb();
    this.getData();
  }


  ionViewDidLoad() {
    if(this.lists == null || this.lists == undefined){
      this.getData();
    }
    // this.getData();
    this.getTotal();
  }

  ionViewDidEnter() {
    if(this.lists == null || this.lists == undefined){
      this.getData();
    }

  }
  updateCheck(){
      if(this.check == false){
        this.check = true;
      }else{
        this.check = false;
      }
  }

  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  getData() {
      this
        .sqliteService
        .getRows()
        .then(s => {
          this.lists = this.sqliteService.arr;
          console.log(this.lists);
          // this.amount = this.lists.reduce
          // this.getTotal();
          for (var i = 0; i < this.lists.length; i++) {
                this.lists = this.lists.map(list =>{
                  list['qty'] = 1;
                  return list;
                });
        }
        }).catch(err=>{
          console.log(err)
      });
  }

  viewList(id){
    console.log(id);
        this.showLoading();
        this.checklist.getItemRows(id).then(res=> {
          this.dismissLoading();
          console.log(res);
          if (res){
            let options = {
              "duration": 500,
              "androiddelay": 0,
              "iosdelay": 0
            };
            this.transition.fade(options).then(res2 => {
              this.navCtrl.push("ItemmodalPage", {items: this.checklist.itemrr,id:id});
            }).catch(e => {
              this.showAlert("Your Item List is empty");
            });
          }else{
            this.showAlert("fail, try again!");
          }
        }).catch(err=>{
          console.log(err);
        });


}

getTotal() {
  let total = 0;
  for (var i = 0; i < this.lists.length; i++) {
      if (this.lists[i].price) {

         let k= this.lists[i].price * this.lists[i].qty;
        total += k;
          // console.log(this.amount);

      }
  }
  return total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
showAmount(value){
  return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
  addToList() {
    // var page = this.modal.create("ListmodalPage");
    // page.present();
        let options = {
          "duration": 500,
          "androiddelay": 0,
          "iosdelay": 0
        };
        this.transition.fade(options).then(res=>
        {this.navCtrl.push("ListmodalPage");})
          .catch(err=>{console.log(err)});

    }

  delete(id) {
    let alert = this.alertCtrl.create();
    alert.setMessage('Are sure you want to delete');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Delete',
      handler: data => {

        this.makedelete(id);
      }
    });
    alert.present();

}

  makedelete(id){
    this.showLoading();
    this.sqliteService.del(id).then(res=>{
      this.dismissLoading();
      if(res){
        this.getData();
        this.showAlert("List deleted successful!");
      }else{
        this.showAlert("try again")
      }

    }).catch(err=>{console.log(err)})
  }

  increment(){
    // this.qty++;
  }
  decrement(){
    // if(this.qty > 0){
    //   this.qty--;
    // }

  }
  goBack(){
    // let view = this.navCtrl.getActive().id;
    // //Write here wherever you wanna do
    // console.log(view);
    this.navCtrl.setRoot("TabsPage");
  }
}
