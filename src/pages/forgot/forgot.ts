import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams,AlertController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the ForgotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {
  email;
  loading:Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public auth:AuthProvider,public loadingCtrl:LoadingController,public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  resetPassword(){
    if(this.email == null || this.email == ''){
      this.showAlert("Login Email is required");
    }else{
      this.showLoading();
      let credential = {"email": this.email};
      this.auth.resetPassword(credential).subscribe(data=>{
          this.showAlert("Recover password details sent, check your email");

        },error1 => {
          this.showAlert("Process Error, try again!");
        }
      );
    }
  }
}
