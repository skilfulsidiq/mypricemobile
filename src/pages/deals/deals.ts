import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the DealsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deals',
  templateUrl: 'deals.html',
})
export class DealsPage {
loading:Loading;
  private products: any; // <- I've added the private keyword
  private items: any;
  deals:any;
  constructor(public navCtrl: NavController,public auth:AuthProvider
    ,public navParam:NavParams,public loadingCtrl:LoadingController) {
    if(this.navParam.get("deal") != undefined || this.navParam.get("deals") != null ){
      this.deals = this.navParam.get("deals");
    }else{
      this.storedeal();
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DealsPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }

  showDeals(id){
    this.showLoading();
    this.auth.loadStoreDeals(id).subscribe(data => {
      this.dismissLoading();
      this.navCtrl.push("DealproductPage", {storedeal: data});

    }, error => {
      this.dismissLoading();
      console.log(error);
    });
  }
  storedeal(){
    this.showLoading();
    this.auth.allDealStore().subscribe(data=>{
      this.dismissLoading();
      this.deals = data;
    },err=>{ this.dismissLoading();console.log(err)});
  }

  initializeItems() {
    this.items = this.products;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
