import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
// import {HomePage} from "../home/home";


/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tabRoot1;
  tabRoot2;
  tabRoot3;
  tabRoot4;
  constructor(public navCtrl: NavController,public navParams:NavParams) {
    this.tabRoot1="HomePage";
    this.tabRoot2 = "CategoryPage";
    this.tabRoot3 = "SearchPage";
    this.tabRoot4 = "DealsPage";
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad TabsPage');
  }

}
