import { Component } from '@angular/core';
import { IonicPage, NavController,  ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProfileditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profiledit',
  templateUrl: 'profiledit.html',
})
export class ProfileditPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileditPage');
  }
  public closeV(){
    this.viewCtrl.dismiss();
  }
  goBack(){
    this.viewCtrl.dismiss();
  }
}
