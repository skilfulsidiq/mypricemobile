import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  NavParams, Toast,
  ToastController
} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import { Diagnostic } from '@ionic-native/diagnostic';


/**
 * Generated class for the MaplistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-maplist',
  templateUrl: 'maplist.html',
})
export class MaplistPage {
  loading:Loading;
  toast:Toast;
  id;
  branchList:any = [];
  items:any = [];
  filteredBranch = [];
  isFiltered:boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams,public auth:AuthProvider
             ,public toastCtrl:ToastController,public loadingCtrl:LoadingController,public diagnostic:Diagnostic,public alertCtrl:AlertController) {
    this.isFiltered = false;
    this.id = this.navParams.get("id");

  }

  ionViewDidLoad() {
    this.getAllbranch(this.id);
    console.log('ionViewDidLoad MaplistPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }

  initializeItems() {
    this.items = this.branchList;

  }
  // setItem(){
  //   this.items = this.filterItem(this.searchTerm);
  // }
  searchBranch(ev: any) {

    // Reset items back to all of the items
    this.initializeItems();


    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        this.isFiltered = true;
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }
  // searchBranch(event) {
  //   if(event.target.value.length > 2) {
  //     var filteredJson = this.branchList.filter(function (row) {
  //       if(row.state.indexOf(event.target.value) != -1) {
  //         return true
  //       } else {
  //         return false;
  //       }
  //     });
  //     this.isFiltered = true;
  //     this.filteredBranch = filteredJson;
  //   }
  // }
  getAllbranch(id) {
    this.showLoading();
    this.auth.listAllBranches(id).subscribe(data => {
        this.dismissLoading();
        this.branchList = data;
        this.isFiltered = false;
      },
      err => {
        console.log(err)
      });
  }

  itemTapped(event, address){
    console.log(address);
    this.diagnostic.isLocationEnabled().then(e=>{
      if(e){this.navCtrl.push("MapPage", {
        address: address
      });}else{
        this.showAlert("Turn on your location Setting");

      }
},err=>{console.log(err)});


  }

}
