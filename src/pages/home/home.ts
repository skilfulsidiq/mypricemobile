import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams, Toast, ToastController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {Observable} from 'rxjs/Rx';
import {BrowserTab} from "@ionic-native/browser-tab";
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {OneSignal} from "@ionic-native/onesignal";
import {ShareDataProvider} from "../../providers/share-data/share-data";
import {Badge} from "@ionic-native/badge";
import { PushProvider } from '../../providers/push/push';




/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
// var products;
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  buttonIcon: string = "heart-outline";
  threeProduct;
  loading:Loading;
  defaultImage = "assets/imgs/preloader.gif";
  private products: any;
  public items: any;
  showfirst:boolean = true;
  searchresult :boolean = false;
  favourites:any;
  user:any;
  image_exists:boolean = false;
  loaded = false;
  // slideOption = {
  //   autoplay:"3000", loop:"true", speed:"5000",pager:true
  // }
  badge=false;
  badgeNumber;
  storeprice:any;
  levelTwoAd:any=[];
  levelThreeAd:any=[];

  deallist;
  toast:Toast;
  recentProduct:any =[];
  notEmpty = false;
  constructor(public navCtrl: NavController, public auth:AuthProvider,public pushpro:PushProvider
    ,public navParam:NavParams,public loadingCtrl:LoadingController,public noticeBadge:Badge,public toastCtrl:ToastController,
    public browser:BrowserTab,public storage:Storage,public inapp:InAppBrowser,public one:OneSignal) {
    this.getBadgeNumber();
      if(this.navParam.get('products') != null){
        this.threeProduct = this.navParam.get('products');
      }else{
        this.auth.loadProduct().subscribe(data => {
          this.threeProduct = data
        },err=>{
          console.log(err);
        });
        // this.threeProduct = this.storage.get('product');
      }


    this.user = this.navParam.get("user");
    this.getDealList();




  }


  ionViewDidLoad() {
    this.getLevelTwoAd();
    this.getLevelThreeAd();
    this.getRecentProduct();
    this.loadAllproduct();

    // this.showFavourites();

    console.log('ionViewDidLoad HomePage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: '.....',
        spinner: 'crescent',
        cssClass:'myLoader'
        // content: `
        //   <div class="custom-spinner-container">
        //     <img src="assets/imgs/bax.gif" class="customloader"/>
        //   </div>`,
        // duration: 5000
      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }

  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }
  goDetails(id){
    this.showLoading();
    console.log(id);

    this.auth.loadAProduct(id).subscribe(data => {
      this.auth.loadThreeStorePrice(id).subscribe(data=>{
        this.dismissLoading();
        this.storeprice = data;
        console.log(data)
      },error1 => {
        console.log();
      });

      this.navCtrl.push("DetailsPage", {oneproduct: data,user:this.user,storeprice:this.storeprice});
    }, error => {
      this.dismissLoading();
      console.log(error);
    })
  }


  viewAll(){
    // this.showLoading();
    this.navCtrl.push("SearchPage",{user:this.user,products:this.threeProduct});
  }
  viewAllDeals(){
    // this.showLoading();
    this.navCtrl.push("DealsPage",{deals:this.deallist});
  }
  loadAllproduct() {
    this.products = this.threeProduct
    // this.showLoading();
    // this.auth.loadProduct().subscribe(data=>{
    //   // this.loading.dismissAll();
    //   this.products  = data;

    //   this.initializeItems();
    // },err=>{
    //   // this.loading.dismissAll();
    // })
  }
  // showFavourites(){
  //   let credential =1;
  //   this.auth.loadFavorites(credential).subscribe(data => {
  //     this.favourites = data;
  //
  //   }, error => {
  //     console.log(error);
  //   });
  // }
    getRecentProduct(){
      this.auth.loadRecentProduct().subscribe(res=>{
        this.recentProduct = res;
      },error1 => {console.log(error1)});
    }

  initializeItems() {
    this.items = this.threeProduct
    // this.products;
  }
  getItems(event) {

    // Reset items back to all of the items
    // this.initializeItems();

    // set val to the value of the searchbar
    let val = event.target.value;
    let value = val.trim();
    console.log(val);
      this.navCtrl.push("HomesearchPage",{value:value});
    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.items = this.items.filter((item) => {
    //     // this.navCtrl.push
    //     return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }
  }

  toggleIcon(getIcon:number) {
    console.log(getIcon);
    let credential = {"userid":this.user.id,"productid":getIcon};
    if (this.buttonIcon === 'heart') {
      console.log(credential.userid+"  "+credential.productid);
      this.auth.notFavourite(getIcon).subscribe(data=>{
        this.buttonIcon = "heart-outline";
      },error1 => {
        console.log(error1);
      });

    }
    else if (this.buttonIcon === 'heart-outline') {
      console.log(credential.userid+"  "+credential.productid);
      this.auth.makeFavourite(credential).subscribe(data=>{
        this.buttonIcon = "heart";
      },error1 => {
        console.log(error1);
      });

    }
  }

  getBadgeNumber(){
    this.badgeNumber = this.pushpro.get();
    console.log(this.badgeNumber);
    if(this.badgeNumber != ''|| this.badgeNumber !=null){
      this.badge = true;
    }else{
      this.badge = false;
    }


  }
  notification(){
    this.badge = false;
    let newmsg = this.pushpro.pushdata;
    if(newmsg != ''){
      this.navCtrl.push("PushPage",{message:newmsg});
    }else{
      this.presentToast("No new Message");
    }

  }

  getLevelTwoAd(){

    Observable
      .interval(60000)
      .timeInterval()
      .flatMap(() =>
        this.auth.levelTwoAd())
      .subscribe(data => {

        if(Object.keys(data).length >=1){
          this.image_exists = true;

          this.levelTwoAd=data;
          console.log(data);
        }

      });
  }
  getLevelThreeAd(){
    Observable
      .interval(60000)
      .timeInterval()
      .flatMap(() => this.auth.levelThreeAd())
      .subscribe(data => {
        if(Object.keys(data).length >=1) {
          this.levelThreeAd = data;
          console.log(data);
        }
      });
  }

  async adlink(url){
    this.showLoading();
    try {
      const isAvailable = await this.browser.isAvailable();
      console.log(isAvailable);
      if(isAvailable){
        this.dismissLoading();
        await this.browser.openUrl(url);
      }
      else{
        const browse = this.inapp.create(url,'_self');
      }
    } catch (error) {
      console.log(error)
    }
  }

  getDealList(){
    this.auth.allDealStore().subscribe(deal=>{
      this.deallist = deal;
    },err=>{console.log(err)});
  }

  dealDetails(id){
    this.showLoading();
    console.log(id);

    this.auth.loadStoreDeals(id).subscribe(data => {
      this.dismissLoading();
      this.navCtrl.push("DealproductPage", {storedeal: data});
    }, error => {
      this.dismissLoading();
      console.log(error);
    })
  }
}
