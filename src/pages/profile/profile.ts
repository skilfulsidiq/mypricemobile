import { Component,ViewChild } from '@angular/core';
import {
  IonicPage,
  Nav,
  NavController,
  ModalController,
  NavParams,
  Platform,
  LoadingController,
  AlertController,
  Loading
} from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {ShareDataProvider} from "../../providers/share-data/share-data";
import { AuthProvider } from './../../providers/auth/auth';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild(Nav) nav: Nav;
  name:string;
  email:string;
  userid:number;
  view:boolean = true;
  isModificationEnable:boolean = false;
  loading:Loading;
  constructor(public navCtrl: NavController,public shareData:ShareDataProvider,public auth:AuthProvider,public loadingCtrl:LoadingController,public alertCtrl:AlertController,
              public navParams: NavParams,public modal:ModalController,public transition:NativePageTransitions) {
  }

  ionViewDidLoad() {
      this.userProfile();
    console.log('ionViewDidLoad ProfilePage');
  }

  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showError(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }


  editProfile(){
    this.view = false;
    this.isModificationEnable = true;
  }
  userProfile(){
      this.shareData.getUserEmail().then(value=>{
        console.log(value);
        this.email = value;
      });
      //get user name
      this.shareData.getUsername().then(value=>{
        console.log(value);
        this.name = value;
      });
      //get userid
      this.shareData.getUserId().then(value=>{
        console.log(value);
        this.userid = value;
      });
      // this.shareData.getUser().then(value=>{
      //   let profile = value;
      //     // for(let i =0; i <=value.lenght)
      //   // console.log(value.user);
      //   console.log("profile "+ value);
      // }).catch(err=>{console.log(err);})
      // this.name = user.name;

    }
    saveProfile(){

        this.showLoading();
      let credential = {"name":this.name,"email":this.email,"userid":this.userid};
        this.auth.updateEmail(credential).subscribe(res=>{
            this.dismissLoading();
          this.isModificationEnable=true;
            this.showError("Profile Updated");
        },err=>{console.log(err); this.dismissLoading();this.showError("try again")})
    }
  goBack(){
      // let view = this.navCtrl.getActive().id;
      //Write here wherever you wanna do
      // console.log(view);
      this.navCtrl.setRoot("TabsPage");
  }
  updateProfile(){
    this.navCtrl.push("ChangepasswordPage",{email:this.email,userid:this.userid});
  }
}
