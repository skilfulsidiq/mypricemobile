import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,LoadingController,
  Loading,AlertController,ToastController,
  Toast, } from 'ionic-angular';
  import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {SqliteProvider} from "../../providers/sqlite/sqlite";

/**
 * Generated class for the ListmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listmodal',
  templateUrl: 'listmodal.html',
})
export class ListmodalPage {
product:string;
shopowner:string;
price:string;
loading:Loading;
toast:Toast;
lists=[];
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController,public alertCtrl:AlertController,
     public navParams: NavParams,public viewCtrl : ViewController,public sqlService:SqliteProvider,public toastCtrl:ToastController) {
    this.sqlService.openDb();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListmodalPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration:3000
      });
      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }


  public closeModal(){
    this.viewCtrl.dismiss();
  }
  addItem(){
    if(this.product == '' || this.shopowner==''|| this.price==''){
      this.showAlert("All field are required!");
    }else{
      this.showLoading();
    //   var today =new Date().toJSON().slice(0,10).replace(/-/g,'/');
        this.sqlService.addItem(this.product,this.shopowner,this.price).then(res=>{
          this.dismissLoading();
          if(res){
              this.product ='';
              this.shopowner = '';
              this.price = '';
              // this.getData();
            this.presentToast("Item added successfully");
          }else{
            this.showAlert("fail, try Again!");
          }

        }).catch(err=>{console.log(err)})
    //
    }
  }
  getData() {
    this
      .sqlService
      .getRows()
      .then(s => {
        this.lists = this.sqlService.arr;
        console.log(this.lists);
        // this.amount = this.lists.reduce
        // this.getTotal();
        for (var i = 0; i < this.lists.length; i++) {
          this.lists = this.lists.map(list =>{
            list['qty'] = 1;
            return list;
          });
        }
      }).catch(err=>{
      console.log(err)
    });
  }
}
