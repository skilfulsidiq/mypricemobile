import { ShareDataProvider } from './../../providers/share-data/share-data';
// import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import {AlertController,
  IonicPage, Loading, LoadingController,
  NavController,
  Toast,
  ToastController,
} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
form;
toast:Toast;
loading:Loading;
// register form
name;
email;
password;
roleid:number = 3;
// end of register form parameter
user;
three;
// socialuser
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  constructor(public navCtrl: NavController,public auth:AuthProvider,
              public alertCtrl:AlertController,public toastCtrl:ToastController,
              public loadingCtrl: LoadingController,private fireAuth:AngularFireAuth,public sharedData:ShareDataProvider,
              public facebook:Facebook, public google:GooglePlus) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  showHide(){
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }
  //alrt
  alert(message: string) {
    this.alertCtrl.create({
      title: 'Failure',
      subTitle: message,
      buttons: [{
        text: 'Ok',
        handler: () => {

        }
      },
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        }]
    }).present();
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }
  register(){
    if(this.name ===  null|| this.email == null || this.password == null){
        this.alert("All field are required");
    }else{
       this.showLoading();
        let credential = {"name":this.name,"email":this.email,"password":this.password,"roleid":3};
        this.auth.register(credential).subscribe(data=>{
          this.dismissLoading();
          this.user = data;
          console.log(data);
          this.sharedData.login(this.user.email,this.user.name,this.user.id);
          this.auth.loadProduct().subscribe(data2 => {
            this.three = data2;
            this.presentToast("welcome "+this.name);
            this.navCtrl.setRoot("TabsPage", {user:this.user, products: this.three});
          });

        },error1 => {
          this.dismissLoading();
          this.presentToast("Error ! Try again !!!");
        });
    }
  }
  facebookLogin():void {
    this.showLoading();
    // this.fireAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then( res=> {
    //     this.dismissLoading();
    //     this.user = res;
    //     this.sharedData.localStorageUser(res.user.displayName,res.user.email);
    //     this.auth.loadProduct().subscribe(data2 => {
    //       this.three = data2;
    //       console.log(data2);
    //       this.presentToast("welcome " + res.user.displayName);
    //       this.presentToast('Success! You\'re Logged in');
    //       this.navCtrl.setRoot("TabsPage", {user: this.user, products: this.three});
    //     });
    //   },
    //     error =>{
    //       this.dismissLoading();
    //       this.presentToast("network Error! try again");
    //       console.log(error);
    //     }
    //
    //   )
      this.facebook.login(['email']).then( (response) => {
      const facebookCredential = firebase.auth.FacebookAuthProvider
        .credential(response.authResponse.accessToken);

      firebase.auth().signInWithCredential(facebookCredential).then((success) => {
          console.log("Firebase success: " + JSON.stringify(success));
          this.dismissLoading();
          console.log(success);
          this.user = success;
          let credential = {"name":this.user.displayName,"email":this.user.email,"password":"facebook","roleid":3};
            // this.auth.register(credential).subscribe(res=>{
              this.auth.loadProduct().subscribe(data2 => {
                this.three = data2;
                console.log(data2);
                this.presentToast("welcome " + this.user.displayName);
                this.presentToast('Success! You\'re Logged in');
                this.navCtrl.setRoot("TabsPage", {user: this.user, products: this.three});
              });
            // });
        })
        .catch((error) => {
          this.dismissLoading();
          console.log("Firebase failure: " + JSON.stringify(error));
        });

    }).catch((error) => { this.dismissLoading(); console.log(error) });



  }

  googleLogin():void {
    this.showLoading();
    // this.fireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then( res=> {
    //     this.dismissLoading();
    //     this.user = res;
    //     this.sharedData.localStorageUser(res.user.displayName,res.user.email);
    //     console.log(res);
    //     this.auth.loadProduct().subscribe(data2 => {
    //       this.three = data2;
    //       this.presentToast("welcome " + res.user.displayName);
    //       this.presentToast('Success! You\'re Logged in');
    //       this.navCtrl.setRoot("TabsPage", {user: this.user, products: this.three});
    //     });
    //   },
    //   error =>{
    //     this.dismissLoading();
    //     this.presentToast("network Error! try again");
    //     console.log(error);
    //   }
    //
    // )
    this.google.login({
      'webClientId': '983621471933-rruqntj62dbmotorgfg84asm1tkssrr5.apps.googleusercontent.com',
      'offline': true,
      // 'scopes':'profile email'
    }).then( res => {
      firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
        .then( success => {
          this.dismissLoading();
          console.log("Firebase success: " + JSON.stringify(success));
          this.user = success;
          this.auth.loadProduct().subscribe(data2 => {
            this.three = data2;
            console.log(data2);
            this.presentToast("welcome " + this.user.displayName);
            this.presentToast('Success! You\'re Logged in');
            this.navCtrl.setRoot("TabsPage", {user: this.user, products: this.three});
          });
        })
        .catch( error => console.log("Firebase failure: " + JSON.stringify(error)));
    }).catch(err => console.error("Error: ", err));
  }



}
