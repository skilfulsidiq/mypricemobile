import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DealproductPage } from './dealproduct';

@NgModule({
  declarations: [
    DealproductPage,
  ],
  imports: [
    IonicPageModule.forChild(DealproductPage),
  ],
})
export class DealproductPageModule {}
