import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the DealproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dealproduct',
  templateUrl: 'dealproduct.html',
})
export class DealproductPage {
  loading:Loading;
  private products: any; // <- I've added the private keyword
  private items: any;
  deals:any;
  constructor(public navCtrl: NavController,public auth:AuthProvider
    ,public navParam:NavParams,public loadingCtrl:LoadingController) {
    this.deals = this.navParam.get("storedeal");
      // this.showDeals();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DealsPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }
  // showDeals(){
  //   this.auth.loadStoreDeals().subscribe(data => {
  //       this.deals = data;
  //
  //   }, error => {
  //     console.log(error);
  //   });
  // }
  goDetails(id){
    this.showLoading();
    console.log(id);

    this.auth.loadAProduct(id).subscribe(data => {
    this.dismissLoading();
      console.log(data);
    this.navCtrl.setRoot("DetailsPage", {oneproduct: data,dealprice:true});
   }, error => {
     this.dismissLoading();
    console.log(error);
    })
  }

  initializeItems() {
    this.items = this.products;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
