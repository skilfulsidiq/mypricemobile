import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,
  LoadingController,  AlertController } from 'ionic-angular';

/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
  oldpassword;
  newpassword;
  email;
  userid;
  loading:Loading;
  constructor(public navCtrl: NavController,public alertCtrl: AlertController,
    public navParams: NavParams,public loadingCtrl:LoadingController,public auth:AuthProvider) {
    this.email = this.navParams.get('email');
    this.userid = this.navParams.get('userid');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }
  showError(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  changePassword(){
    if (this.email == null || this.oldpassword == null || this.newpassword == null){
        this.showError("All field are required!");
    } else{
        this.showLoading();
        let credential = {"email":this.email,"oldpassword":this.oldpassword,"newpassword":this.newpassword};
        this.auth.changePassword(credential).subscribe(data=>{
            this.dismissLoading();
            this.showError("Your password has been changed");
            this.email = "";
            this.oldpassword = "";
            this.newpassword = "";
        },error=>{
          this.dismissLoading();
          this.showError("process error! try again!");
        });
    }
  }
}
