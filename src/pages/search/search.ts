import { Component,ViewChild ,ElementRef} from '@angular/core';
import {
  Events,
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  NavParams,
  Toast,
  ToastController
} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
// import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import {FavouriteProvider} from "../../providers/favourite/favourite";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild('name') iconName: ElementRef;
  searching: any = false;
  // searchTerm:string;
  loading: Loading;
  error = false;
 public products: any;
  catproduct:any = [];
  items:any =[];
  suggestion:any = [];
  storeprice;
  user:any;
  toast:Toast;
  searchIsActive:boolean ;
  searchProduct:any = [];
  initialButtonIcon = "ios-heart-outline";
  // buttonIcon = "ios-heart-outline";
  constructor(public navCtrl: NavController, public auth: AuthProvider,public toastCtrl:ToastController,
              public loadingCtrl: LoadingController,public navparam:NavParams,public event:Events,public favour:FavouriteProvider) {
    this.favour.openDb();
    this.searchIsActive = false;
    // this.iconName.nativeElement
    this.catproduct = this.navparam.get("catproduct");
    if (this.catproduct === undefined || this.catproduct.length == 0){
          this.loadAllproduct();
    }else{
        this.products = this.catproduct;
    }
    // this.initializeItems();
    this.loadsuggestion();

  }

  ionViewDidLoad() {
    // this.setItem();
    console.log('ionViewDidLoad SearchPage');
  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        // this.loading.setDuration(1000)
        this.loading = null;
    }

  }
  loadsuggestion(){
    this.auth.loadSuggestion().subscribe(data=>{
      this.suggestion = data;
      console.log(this.items);
    },err=>{console.log(err)});
  }
  goDetail(id) {
    console.log(id);
   this.showLoading();
   this.auth.loadAProduct(id).subscribe(data => {
    this.auth.loadThreeStorePrice(id).subscribe(data2=>{
      this.dismissLoading();
      this.storeprice = data2;
      console.log(data)
    },error1 => {
      console.log();
    });

      this.navCtrl.setRoot("DetailsPage", {oneproduct: data,user:this.user,storeprice:this.storeprice});
    }, error => {
      console.log(error);
    })

  }

    initializeItems(){
      this.items = this.suggestion;
      // this.items = this.products;
      // console.log(this.items.name);
    }
  loadAllproduct() {
    this.showLoading();
      this.auth.loadProduct().subscribe(data=>{
        this.dismissLoading();
            this.products  = data;
          console.log("all product from search page "+ this.products);
        // this.initializeItems();


      },err=>{
        // this.loading.dismissAll();
      })
  }
  toType(event){
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar

    // set val to the value of the searchbar
    const val = event.target.value;
    console.log(val);
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      this.searchIsActive = true;
    } else {
      this.searchIsActive = false;
      // hide the results when the query is empty
      // this.searchIsActive = false;
      // this.loadAllproduct();
    }

  }
  toClear(event){
    this.searchIsActive = false;
  }
  ionCancel(event){
    this.searchIsActive = false;
  }
  getItems(event) {
    // Reset items back to all of the items
    // this.initializeItems();

    // set val to the value of the searchbar
    let val = event.target.value;
    let value = val.trim();
    console.log(value);
    // this.searchResult(value);
    this.navCtrl.push("HomesearchPage",{value:value});

  }
  selected(product){
    console.log("suggetsion click "+ product);
    this.navCtrl.push("HomesearchPage",{value:product});
  }

  addToFavour(name,image,price,store,id,product){
    this.favour.addFavourite(name,image,price,store,id).then(res=>{
      if(res){

        if(product.icon == "heart"){
            this.favour.del(id).then(yes=>{
              if(yes){
                product.icon = "ios-heart-outline";
                this.presentToast("remove from favourite");
              }
            },err=>{console.log(err)});
        }else{
          product.icon = "heart";
          this.presentToast("added to Favourite");
        }

      }
    }).catch(err=>{console.log(err)})
  }
  // toggleIcon(getIcon:number) {
  //   if (this.buttonIcon === 'heart') {
  //
  //
  //   }
  //   else if (this.buttonIcon === 'heart-outline') {
  //
  //
  //   }
  // }
}
