import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {

  loading:Loading;
  category;
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController,
              public auth:AuthProvider) {
    this.loadCategory();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }
  goDetail(id){
       this.showLoading()
      this.auth.loadProductFromCategory(id).subscribe(data=>{
        console.log(data);
            this.dismissLoading();
        this.navCtrl.setRoot("SearchPage",{catproduct:data});
      },error1 => {
        this.dismissLoading();
      })

  }

  loadCategory(){
  this.showLoading();
    this.auth.getCategory().subscribe(data=>{
      this.dismissLoading();
      this.category = data;
    },error1 => {
      this.dismissLoading();
    });
  }
}
