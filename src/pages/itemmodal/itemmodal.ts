import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  Loading,
  LoadingController,
  NavController,
  NavParams, Toast, ToastController,
  ViewController
} from 'ionic-angular';
import {ChecklistProvider} from "../../providers/checklist/checklist";
import {SqliteProvider} from "../../providers/sqlite/sqlite";

/**
 * Generated class for the ItemmodalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-itemmodal',
  templateUrl: 'itemmodal.html',
})
export class ItemmodalPage {
items:any= [];
lists = [];
listid;
product:string;
price;
loading:Loading;
toast:Toast;
addCheck:boolean = false;
id;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl : ViewController,public sqliteService:SqliteProvider,
              public checklist:ChecklistProvider,public loadingCtrl:LoadingController,public alertCtrl:AlertController,public toastCtrl:ToastController) {
    this.checklist.openDb();
    this.id = this.navParams.get("id");
      this.items = this.navParams.get("items");
      console.log(this.items);

  }

  ionViewDidLoad() {
    this.getData();
    // this.getItemData(this.id);
    // if(this.items == null){
    //   this.getItemData(this.id);
    // }


    console.log('ionViewDidLoad ItemmodalPage');
  }
  ionViewDidEnter() {
    if (this.lists == null){
      this.getData();
    }
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration:3000
      });
      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }
  public closeModal(){
    this.viewCtrl.dismiss();
  }
  public add(){
    this.addCheck = true;
  }
  public view(){
    this.getItemData(this.id);
    this.addCheck = false;
  }

  //functional methods
  getData() {
    this
      .sqliteService
      .getRows()
      .then(s => {
        this.lists = this.sqliteService.arr;
      }).catch(err=>{
      console.log(err)
    });
  }
  getItemData(id){
    this.checklist.getItemRows(id).then(s => {
      console.log(s);

      if(s){
        this.items = this.checklist.itemrr;
      }else{
        this.showAlert("Error, fetching data!, Refresh");
      }

    }).catch(err=>{
      console.log(err)
    });
  }
  public addItem(){
    if (this.listid == null || this.product == null || this.price ==null){
      this.showAlert("All fields are required !");
    } else {
      this.showLoading();
      this.checklist.addItem(this.listid, this.product, this.price).then(res => {
        this.dismissLoading();
        if (res) {
          this.listid = '';
          this.product = '';
          this.price = '';
            this.getItemData(this.id);
          this.presentToast(this.product + " added to shopinglist");
        } else {
          this.showAlert("fail, try again");
        }
      }).catch(err => {
        console.log(err)
      });
    }
  }
  delete(id) {
    let alert = this.alertCtrl.create();
    alert.setMessage('Are sure you want to delete');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Delete',
      handler: data => {

        this.makedelete(id);
      }
    });
    alert.present();

  }

  makedelete(id){
    this.checklist.del(id).then(res=>{
      if(res){
          this.getData();
        this.showAlert("item deleted successful!");
      }else{
        this.showAlert("Try again!");
      }

    }).catch(err=>{console.log(err)})
  }
}
