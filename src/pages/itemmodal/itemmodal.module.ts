import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemmodalPage } from './itemmodal';

@NgModule({
  declarations: [
    ItemmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(ItemmodalPage),
  ],
})
export class ItemmodalPageModule {}
