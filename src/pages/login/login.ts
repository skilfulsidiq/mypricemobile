import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  LoadingController,
  Loading,
  ToastController,
  Toast,
  AlertController
} from 'ionic-angular';
import { Events } from 'ionic-angular';
import {ShareDataProvider} from "../../providers/share-data/share-data";
import {UserProvider} from "../../providers/user/user";
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
loading : Loading;
toast:Toast;
email;
password;
user ;
three;
adOne:any = [];
adTwo:any = [];
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  constructor(public navCtrl: NavController,public auth: AuthProvider,
    public loadingCtrl:LoadingController,public toastCtrl:ToastController,
              public alertCtrl:AlertController,public event:Events,public sharedData:ShareDataProvider) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  showHide(){
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  presentToast(message: string) {
    this.toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      cssClass: "toast-msg",
      //  dismissOnPageChange:true
    });
    this.toast.present();
  }
  //alrt
  alert(message: string) {
    this.alertCtrl.create({
      title: 'Failure',
      subTitle: message,
      buttons: [{
        text: 'Ok',
        handler: () => {

        }
      },
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        }]
    }).present();
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }

  }

  showError(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }



  async login(){
    // this.navCtrl.setRoot("TabsPage");
    if(this.email === null || this.password == null){
      this.alert("email and password are required");

    }else {
    this.showLoading();

      let credential = {"email": this.email, "password": this.password};
    await this.auth.login(credential).subscribe(data => {
          this.dismissLoading();
          if(data){
            this.user = data;
            console.log(this.user);
              // store user email and password to local storage
              // this.localStorageUser(data.name,data.email,data.password);
            this.sharedData.login(this.user.email,this.user.name,this.user.id);
            // console.log()
            this.auth.loadProduct().subscribe(data2 => {
              this.three = data2;
              this.presentToast("welcome "+ this.user.name);
              this.navCtrl.setRoot("TabsPage", {user:this.user, products: this.three,adOne:this.adOne,adTwo:this.adTwo});
            });
          }else{
            this.showError("Access Denied !");
          }

      },error1 => {
        this.dismissLoading();
        this.showError("try again!");
      });

    }


  }
  bypass(){
    this.navCtrl.setRoot("TabsPage");
  }
  // firstThreeProduct(three){


  goRegister(){
    this.navCtrl.push("RegisterPage");
  }
  goForgot(){
    this.navCtrl.push("ForgotPage");
  }

}
