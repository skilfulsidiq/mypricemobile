import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
// import {DetailsPage} from "../details/details";

/**
 * Generated class for the ProductlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productlist',
  templateUrl: 'productlist.html',
})
export class ProductlistPage {
  loading: Loading;
  error = false;

  private products: any; // <- I've added the private keyword
  // private searchQuery: string = '';
  public items: any;
  constructor(public navCtrl: NavController, public auth: AuthProvider,
              public loadingCtrl: LoadingController,public navparam:NavParams) {
    this.products = this.navparam.get("catproduct");
    this.initializeItems();

  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad SearchPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }

  goDetail(id) {
    console.log(id);
    this.showLoading();
    this.auth.loadAProduct(id).subscribe(data => {
      this.dismissLoading();
      this.navCtrl.setRoot("DetailsPage", {oneproduct: data});
    }, error => {
      console.log(error);
    })

  }

  initializeItems() {
    this.items = this.products;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }



}
