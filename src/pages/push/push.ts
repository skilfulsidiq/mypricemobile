import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import * as firebase from "firebase/app";
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the PushPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-push',
  templateUrl: 'push.html',
})
export class PushPage {
  pushMessage: string = '';
  title:string = '';
  loading:Loading;
  notification;
  ready = false;
  constructor(public params: NavParams,public navCtrl:NavController,public auth:AuthProvider,
              public loadingCtrl:LoadingController) {
        this.getNotification();
      let message= params.get("message");
      if(message != ''){
        this.title = message.title;
        this.pushMessage = message.body;
        this.ready = true;
      }


      console.log(this.pushMessage);

  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad PushPage');
  }
 getNotification(){
    this.showLoading();
    this.auth.getAllNotification().subscribe(data=>{
        this.dismissLoading();
        if(data !=false || data !=null){
          this.notification = data;
          this.ready = true;
        }else{
          this.ready = false;
        }

    },err=>{console.log(err)})
 }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration:600
      });
      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
}
