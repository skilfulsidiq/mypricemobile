import { Component } from '@angular/core';
import {AlertController, IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {RecentviewProvider} from "../../providers/recentview/recentview";

/**
 * Generated class for the RecentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recent',
  templateUrl: 'recent.html',
})
export class RecentPage {
  recentList:any = [];
  loading:Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams,public recent:RecentviewProvider,public alertCtrl:AlertController,public loadingCtrl:LoadingController) {
    this.recent.openDb();
  }

  ionViewDidLoad() {
    this.getALlRecent()
    console.log('ionViewDidLoad RecentPage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  getALlRecent(){
      this.recent.getRows().then(res=>{
        if(res){
          this.recentList = this.recent.reviewArray;
          console.log(this.recentList);
        }

      }).catch(err=>{console.log(err)});
  }
  removeAll(){
    let alert = this.alertCtrl.create();
    alert.setMessage('Are sure you want to delete');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Delete',
      handler: data => {

        this.deleteAll();
      }
    });
    alert.present();
  }

  deleteAll(){
    this.recent.delAll().then(res=>{
      if(res){
        this.getALlRecent();
      }
    }).catch(err=>{console.log(err)})
  }
  delete(id,status:boolean = false) {
    let alert = this.alertCtrl.create();
    alert.setMessage('Are sure you want to delete');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Delete',
      handler: data => {
              if(status){
                this.deleteAll();
              }else{
                this.makedelete(id);
              }

      }
    });
    alert.present();

  }

  makedelete(id){
    this.showLoading();
    this.recent.del(id).then(res=>{
      this.dismissLoading();
      if(res){
        this.getALlRecent();
        this.showAlert("List deleted successful!");
      }else{
        this.showAlert("try again")
      }

    }).catch(err=>{console.log(err)})
  }
  goBack(){
    let view = this.navCtrl.getActive().id;
    //Write here wherever you wanna do
    console.log(view);
    this.navCtrl.setRoot("TabsPage");
  }

}
