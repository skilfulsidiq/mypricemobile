import { Component } from '@angular/core';
import {AlertController, IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {FavouriteProvider} from "../../providers/favourite/favourite";

/**
 * Generated class for the FavouritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favourite',
  templateUrl: 'favourite.html',
})
export class FavouritePage {
    favourList:any = [];
    loading:Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams,public favour:FavouriteProvider,
              public alertCtrl:AlertController,public loadingCtrl:LoadingController) {
    this.favour.openDb();
  }

  ionViewDidLoad() {
    this.getALlFavour()
    console.log('ionViewDidLoad FavouritePage');
  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        // content: 'Please wait...',
        spinner: 'crescent',
        cssClass:'myLoader'

      });

      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }
  showAlert(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
  getALlFavour(){
    this.favour.getRows().then(res=>{
      if(res){
        this.favourList = this.favour.favouriteArray;
        console.log(this.favourList);
      }

    }).catch(err=>{console.log(err)});
  }
  removeAll(){
    let alert = this.alertCtrl.create();
    alert.setMessage('Are sure you want to delete');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Delete',
      handler: data => {

        this.deleteAll();
      }
    });
    alert.present();
  }
  deleteAll(){
    this.favour.delAll().then(res=>{
      if(res){
        this.getALlFavour();
      }
    }).catch(err=>{console.log(err)})
  }

  goBack(){
    let view = this.navCtrl.getActive().id;
    //Write here wherever you wanna do
    console.log(view);
    this.navCtrl.setRoot("TabsPage");
  }
  delete(id) {
    let alert = this.alertCtrl.create();
    alert.setMessage('Are sure you want to delete');
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Delete',
      handler: data => {

        this.makedelete(id);
      }
    });
    alert.present();

  }

  makedelete(id){
    this.showLoading();
    this.favour.del(id).then(res=>{
      this.dismissLoading();
      if(res){
        this.getALlFavour();
        this.showAlert("List deleted successful!");
      }else{
        this.showAlert("try again")
      }

    }).catch(err=>{console.log(err)})
  }
}
