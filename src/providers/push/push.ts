import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';
import {Badge} from "@ionic-native/badge";

/*
  Generated class for the PushProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PushProvider {

  pushdata;
  number;
  constructor(public badge:Badge) {

  }
  async get(){
    try {
      let badgeAmount = await this.badge.get();
      console.log(badgeAmount);
      return badgeAmount;
    }catch (e) {
      console.log(e)
    }
  }
  async clearAll(){
   await this.badge.clear();
  }

  async increaseBadges(badgeNumber){
    try{
      let badgek = await this.badge.increase(Number(badgeNumber));
      console.log(badgek);
    }catch(e){
      console.log(e);
    }
  }

  async setBadges(badgeNumber:number, data){
    try {
      let d = await this.badge.set(badgeNumber);
      this.pushdata = data;
      console.log(d);
    }catch (e) {
      console.log(e);
    }
  }
}
