import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the SqliteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
declare var window : any;
@Injectable()
export class SqliteProvider {
  public text : string = "";
  public db = null;
  public arr = [];
  itemrr = [];
  constructor(public sql:SQLite) {}
  /**
   *
   * Open The Datebase
   */
  openDb() {
    // this.sql.create({
    //   name: 'ionicdb.db',
    // location: 'default'
    // }).then((db: SQLiteObject) => {
    //   db.executeSql('CREATE TABLE IF NOT EXISTS expense(rowid INTEGER PRIMARY KEY, date TEXT, type TEXT, description TEXT, amount INT)', {})
    this.db = window
      .sqlitePlugin
      .openDatabase({name: 'myprice.db', location: 'default'});
    this
      .db
      .transaction((tx) => {
        tx.executeSql('CREATE TABLE IF NOT EXISTS shoplists(id INTEGER PRIMARY KEY AUTOINCREMENT,product TEXT, store TEXT,price INTEGER)');
      }, (e) => {
        console.log('Transtion Error', e);
      }, () => {
        console.log('Populated Datebase OK..');
      })
  }
  /**
   *
   * @param addItem for adding: function
   */
  addItem(product,store,price) {

    return new Promise(resolve => {
      var InsertQuery = "INSERT INTO shoplists VALUES(NULL,?,?,?)";
      this
        .db
        .executeSql(InsertQuery, [product,store,price], (r) => {
          console.log('Inserted... Sucess..', product);
          this
            .getRows()
            .then(s => {
              resolve(true)
            });
        }, e => {
          console.log('Inserted Error', e);
          resolve(false);
        })
    })
  }

  //Refresh everytime

  getRows() {
    return new Promise(res => {
      this.arr = [];
      let query = "SELECT * FROM shoplists";
      this
        .db
        .executeSql(query, [], rs => {
          if (rs.rows.length > 0) {
            for (var i = 0; i < rs.rows.length; i++) {
              var item = rs
                .rows
                .item(i);
              this
                .arr
                .push(item);
            }
          }
          res(true);
        }, (e) => {
          console.log('Sql Query Error', e);
        });
    })

  }
  //to delete any Item
  del(id) {
    return new Promise(resolve => {
      var query = "DELETE FROM shoplists WHERE id=?";
      this
        .db
        .executeSql(query, [id], (s) => {
          console.log('Delete Success...', s);
          this
            .getRows()
            .then(s => {
              resolve(true);
            });
        }, (err) => {
          console.log('Deleting Error', err);
        });
    })

  }
  //to Update any Item
  update(id, txt) {
    return new Promise(res => {
      var query = "UPDATE lists SET todoItem=?  WHERE id=?";
      this
        .db
        .executeSql(query, [
          txt, id
        ], (s) => {
          console.log('Update Success...', s);
          this
            .getRows()
            .then(s => {
              res(true);
            });
        }, (err) => {
          console.log('Updating Error', err);
        });
    })

  }

  //  delete if
  getItemRows(id) {
    return new Promise(res => {
      this.arr = [];
      let query = "SELECT * FROM lists WHERE id=?";
      this
        .db
        .executeSql(query, [id], rs => {
          if (rs.rows.length > 0) {
            for (var i = 0; i < rs.rows.length; i++) {
              var item = rs
                .rows
                .item(i);
              this
                .itemrr
                .push(item);
            }
          }
          res(true);
        }, (e) => {
          console.log('Sql Query Error', e);
        });
    })

  }
}
