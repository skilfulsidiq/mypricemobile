import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLite} from "@ionic-native/sqlite";

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
declare var window : any;
@Injectable()
export class UserProvider {
  public db = null;
  public User = [];
  itemrr = [];
  constructor(public sql:SQLite) {
    console.log('Hello FavouriteProvider Provider');
  }


  openDb() {
    // this.sql.create({
    //   name: 'ionicdb.db',
    // location: 'default'
    // }).then((db: SQLiteObject) => {
    //   db.executeSql('CREATE TABLE IF NOT EXISTS expense(rowid INTEGER PRIMARY KEY, date TEXT, type TEXT, description TEXT, amount INT)', {})
    this.db = window
      .sqlitePlugin
      .openDatabase({name: 'myprice.db', location: 'default'});
    this
      .db
      .transaction((tx) => {
        tx.executeSql('CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, email TEXT,userid INTEGER)');
      }, (e) => {
        console.log('Transtion Error', e);
      }, () => {
        console.log('Populated Datebase OK..');
      })
  }
  addUser(name,email,userid) {

    return new Promise(resolve => {
      var InsertQuery = "INSERT INTO user VALUES(NULL,?,?,?)";
      this
        .db
        .executeSql(InsertQuery, [name,email,userid], (r) => {
          console.log('Inserted... Sucess..', name);
          this
            .getUser()
            .then(s => {
              resolve(true)
            });
        }, e => {
          console.log('Inserted Error', e);
          resolve(false);
        })
    })
  }

  getUser() {
    return new Promise(res => {
      // this.User= [];
      let query = "SELECT * FROM user";
      this
        .db
        .executeSql(query, [], rs => {
          if (rs.rows.length > 0) {
            this.User = [];
            for (var i = 0; i < rs.rows.length; i++) {
              var item = rs.rows.item(i);
              console.log("this is user "+item);
              this.User.push(item);
            }
          }
          res(true);
        }, (e) => {
          console.log('Sql Query Error', e);
        });
    })

  }

}
