import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ChecklistProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
declare var window : any;
@Injectable()
export class ChecklistProvider {
  public text : string = "";
  public db = null;
  public arr = [];
  public itemrr = [];
  constructor(public http: HttpClient) {
    console.log('Hello ChecklistProvider Provider');
  }
  openDb() {
    this.db = window
      .sqlitePlugin
      .openDatabase({name: 'myprice.db', location: 'default'});
    this
      .db
      .transaction((tx) => {
        tx.executeSql('CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY,listid,product TEXT, price TEXT)');
      }, (e) => {
        console.log('Transtion Error', e);
      }, () => {
        console.log('Populated Datebase OK..');
      })
  }
  /**
   *
   * @param addItem for adding: function
   */
  addItem(listid,product,price) {

    return new Promise(resolve => {
      var InsertQuery = "INSERT INTO items VALUES(null,?,?,?)";
      this
        .db
        .executeSql(InsertQuery, [listid,product,price], (r) => {
          console.log('Inserted... Sucess..', r);
          this
            .getRows()
            .then(s => {
              resolve(true)
            });
        }, e => {
          console.log('Inserted Error', e);
          resolve(false);
        })
    })
  }

  //Refresh everytime

  getRows() {
    return new Promise(res => {
      this.arr = [];
      let query = "SELECT * FROM items";
      this
        .db
        .executeSql(query, [], rs => {
          if (rs.rows.length > 0) {
            for (var i = 0; i < rs.rows.length; i++) {
              var item = rs
                .rows
                .item(i);
              this
                .arr
                .push(item);
            }
          }
          res(true);
        }, (e) => {
          console.log('Sql Query Error', e);
        });
    })

  }
  //to delete any Item
  del(id) {
    return new Promise(resolve => {
      var query = "DELETE FROM items WHERE id=?";
      this
        .db
        .executeSql(query, [id], (s) => {
          console.log('Delete Success...', s);
          this
            .getRows()
            .then(s => {
              resolve(true);
            });
        }, (err) => {
          console.log('Deleting Error', err);
        });
    })

  }
  //to Update any Item
  update(id, listid,product,price) {
    return new Promise(res => {
      var query = "UPDATE items SET listid=?,product=?,price=?  WHERE id=?";
      this
        .db
        .executeSql(query, [
          listid,product,price, id
        ], (s) => {
          console.log('Update Success...', s);
          this
            .getRows()
            .then(s => {
              res(true);
            });
        }, (err) => {
          console.log('Updating Error', err);
        });
    })

  }

  getItemRows(id) {
    // { name: data.rows.item(i).name, skill: data.rows.item(i).skill, yearsOfExperience: data.rows.item(i).yearsOfExperience }
    return new Promise(res => {
      this.itemrr = [];
      let query = "SELECT * FROM items WHERE listid=?";
      this
        .db
        .executeSql(query, [id], rs => {
          if (rs.rows.length > 0) {
            for (var i = 0; i < rs.rows.length; i++) {
              var item = rs
                .rows
                .item(i);
              this
                .itemrr
                .push(item);
            }
          }

          res(true);
        }, (e) => {
          console.log('Sql Query Error', e);
        });
    })

  }
  delAll(){
    return new Promise(resolve => {
      // this.db.delete("favourites",null,null);
      var query = "DELETE FROM items";
      this
        .db
        .executeSql(query,  (s) => {
          console.log('Delete Success...', s);
          this.db.executeSql("vacuum",()=>{
            resolve(true);
          });
        }, (err) => {
          console.log('Deleting Error', err);
        });
    })
  }
}

