import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Storage} from "@ionic/storage";

/*
  Generated class for the ShareDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ShareDataProvider {
  name:string;
  email:string;
    hasLogged = "hasLoggedIn";
    constructor(public http: HttpClient,public storage:Storage) {

      console.log('Hello ShareDataProvider Provider');
    }




  setUsername(name:string):void{
    this.storage.set('name', name);
  }
  setUserEmail(email: string): void {
    this.storage.set('email', email);
  };
    setUserId(userid:number):void{
      this.storage.set('userid',userid);
    }
  setUser(name:string,email:string,userid:number){
    let user = {name:name,email:email,userid:userid};
    this.storage.set('user',user);
    this.storage.set(this.hasLogged, true);
  }
  getUser():Promise<object>{
    return this.storage.get('user').then((value) => {
      return value;
    });
  }
  getUsername(): Promise<string> {
    return this.storage.get('name').then((value) => {
      return value;
    });
  }
  getUserEmail(): Promise<string> {
    return this.storage.get('email').then((value) => {
      return value;
    });
  }
  getUserId(): Promise<number> {
    return this.storage.get('userid').then((value) => {
      return value;
    });
  }
  login(email: string,name:string,userid:number): void {
    this.storage.set(this.hasLogged, true);
    this.setUsername(name);
    this.setUserEmail(email);
    this.setUserId(userid);
  };

  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.hasLogged).then((value) => {
      return value === true;
    });
  }
  logout(): void {
    this.storage.remove(this.hasLogged);
    this.storage.remove('email');
    this.storage.remove('name');
  };

  saveNotice(){
    this.storage.set('notice',true);
  }
  removeNotice(){
    this.storage.remove('notice');
  }
}
