import { HttpClient } from '@angular/common/http';
import { Injectable,ViewChild } from '@angular/core';
import {Network} from "@ionic-native/network";
import {NavController,Toast, ToastController,Events,Platform} from "ionic-angular";
import { BehaviorSubject } from "rxjs/Rx";



@Injectable()
export class NetworkProvider {
@ViewChild('Nav') nav:NavController;
  previousStatus;
  public connected: BehaviorSubject<boolean> = new BehaviorSubject(true);
  private connectionToast: Toast;
  private subscribedToNetworkStatus: boolean = false;
  constructor(private network: Network,public toastCtrl:ToastController,public eventCtrl:Events,private platform: Platform) {
    console.log('Hello NetworkProvider Provider');

  }

  // public initializeNetworkEvents(): void {
  //   this.network.onDisconnect().subscribe(() => {
  //     if (this.previousStatus === ConnectionStatusEnum.Online) {
  //       this.eventCtrl.publish('network:offline');
  //     }
  //     this.previousStatus = ConnectionStatusEnum.Offline;
  //   });
  //   this.network.onConnect().subscribe(() => {
  //     if (this.previousStatus === ConnectionStatusEnum.Offline) {
  //       this.eventCtrl.publish('network:online');
  //     }
  //     this.previousStatus = ConnectionStatusEnum.Online;
  //   });
  // }



    // presentToast(msg:string){
    //   var displayedTime = new Date().getTime();
    //   // let duration: number = 3500;
    //   let toast = this.toastCtrl.create({
    //     message: msg,
    //     showCloseButton: true,
    //     // closeButtonText: btn,
    //     dismissOnPageChange: false,
    //   });
    //
    //   // toast.onDidDismiss(() => {
    //   //   let dismissedTime = new Date().getTime();
    //   //   if ((displayedTime + duration) > dismissedTime) {
    //   //     callback()
    //   //   }
    //   // });
    //
    //   toast.present();
    // }

    // refreshPage(){
    //   //get the currently active page component
    //   var component = this.nav.getActive().instance;
    //     //re-run the view load function if the page has one declared
    //   if (component.ionViewDidLoad) {
    //     component.ionViewDidLoad();
    //   }
    // }

  public setSubscriptions() {
    if (!this.subscribedToNetworkStatus && this.platform.is("cordova")) {
      this.subscribedToNetworkStatus = true;

      if ("none" === this.network.type) {
        this.showAlert();
      }

      this.network.onConnect().subscribe((val) => {
        this.connected.next(true);
        this.connectionToast.dismiss();
        console.log("Network onConnect", val);
      });
      this.network.onchange().subscribe((val) => {
        console.log("Network onchange", val);
      });
      this.network.onDisconnect().subscribe((val) => {
        this.connected.next(false);
        this.showAlert();
      });
    }
  }

    private showAlert() {
      this.connectionToast = this.toastCtrl.create({
        cssClass: "error",
        dismissOnPageChange: false,
        message: "No Internet Connection",
        position: "bottom",
        showCloseButton: false,
      });
      this.connectionToast.present();
    }



}


