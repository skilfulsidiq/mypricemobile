import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
//import {Response} from "@angular/http";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var headers = new HttpHeaders();
headers.append('Access-Control-Allow-Origin' , '*');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
headers.append('Accept','application/json');
headers.append('content-type','application/json');

// let authUrl = "http://localhost/myprice/public/api/";
// let authUrl = "http://www.cirrusairsupport.com/myprice/api/";
// let authUrl="http://myprice.707digital.com/api/";
let authUrl="http://myprice.707digital.com/api/v2018";
@Injectable()
export class AuthProvider {

  constructor(public http: HttpClient) {
    console.log('Hello AuthProvider Provider');
  }

  //authentication apis
  login(credential) {
    return this.http.post(authUrl + 'login', credential, {headers: headers});
  }



  register(credential) {
    return this.http.post(authUrl + 'register', credential, {headers: headers});
  }

  loadCategory() {
    return this.http.get(authUrl+"getCategory",{headers:headers});
  }



  loadProduct() {
    return this.http.get(authUrl+"getAllProduct",{headers:headers});
  }

  getCategory(){
    return this.http.get(authUrl+"getCategory",{headers: headers});
  }
  loadThreeProducts(){
    return this.http.get(authUrl+'getThreeProducts',{headers:headers});

  }

  loadAProduct(id){
    return this.http.get(authUrl+"getAProduct/"+id,{headers:headers});
  }
  loadProductFromCategory(id){
    return this.http.get(authUrl+"getAllProductsFromACategory/"+id,{headers:headers});
  }

  loadThreeStorePrice(name){
    return this.http.get(authUrl+"getStorePrice/"+name,{headers:headers});
  }

  loadDeals(){
    return this.http.get(authUrl+"getAllDeals",{headers:headers});
  }

  loadFavorites(credential){
    return this.http.get(authUrl+"favourite/"+credential+"/favour",{headers:headers});
  }
  makeFavourite(credential){
    console.log(credential);
    return this.http.post(authUrl+"makeFavourite",credential,{headers:headers});
  }
  notFavourite(credential){
    return this.http.get(authUrl+"notFavourite/"+credential,{headers:headers});
  }

  resetPassword(credential){
    return this.http.post(authUrl+"passwordReset",credential,{headers:headers
    });
  }
  changePassword(credential){
    return this.http.post(authUrl+"changePassword",credential,{headers:headers
    });
  }


  levelTwoAd(){
    return this.http.get(authUrl+"getLevelTwoAdvert",{headers:headers});
  }
  levelThreeAd(){
    return this.http.get(authUrl+"getLevelThreeAdvert",{headers:headers});
  }

  allDealStore(){
    return this.http.get(authUrl+"getStoreDeals",{headers:headers});
  }
  loadStoreDeals(id){
    return this.http.get(authUrl+"getAllProductsFromDeal/"+id,{headers:headers});
  }

  listAllBranches(id){
    return this.http.get(authUrl+"listBranches/"+id,{headers:headers});
  }
  getAllNotification(){
    return this.http.get(authUrl+"getAllNotification",{headers:headers});
  }

  searchProduct(value){
    return this.http.get(authUrl+"search/"+value,{headers:headers});
  }
  loadRecentProduct(){
    return this.http.get(authUrl+"recently",{headers:headers});
  }
  loadSuggestion(){
    return this.http.get(authUrl+"suggest",{headers:headers});
  }
  updateEmail(credential){
    return this.http.post(authUrl+"updateEmail",credential,{headers:headers
    });
  }
}
