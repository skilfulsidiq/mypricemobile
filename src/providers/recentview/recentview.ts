import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {SQLite} from "@ionic-native/sqlite";

/*
  Generated class for the RecentviewProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
declare var window : any;
@Injectable()
export class RecentviewProvider {

  public db = null;
  public reviewArray = [];
  itemrr = [];
  constructor(public sql:SQLite) {
    console.log('Hello FavouriteProvider Provider');
  }


  openDb() {
    // this.sql.create({
    //   name: 'ionicdb.db',
    // location: 'default'
    // }).then((db: SQLiteObject) => {
    //   db.executeSql('CREATE TABLE IF NOT EXISTS expense(rowid INTEGER PRIMARY KEY, date TEXT, type TEXT, description TEXT, amount INT)', {})
    this.db = window
      .sqlitePlugin
      .openDatabase({name: 'myprice.db', location: 'default'});
    this
      .db
      .transaction((tx) => {
        tx.executeSql('CREATE TABLE IF NOT EXISTS recentviews(id INTEGER PRIMARY KEY AUTOINCREMENT,productname TEXT, productimage TEXT,price INTEGER,store TEXT,productid INTEGER)');
      }, (e) => {
        console.log('Transtion Error', e);
      }, () => {
        console.log('Populated Datebase OK..');
      })
  }
  addRecentView(productname,productimage,price,store,productid) {

    return new Promise(resolve => {
      var InsertQuery = "INSERT INTO recentviews VALUES(NULL,?,?,?,?,?)";
      this
        .db
        .executeSql(InsertQuery, [productname,productimage,price,store,productid], (r) => {
          console.log('Inserted... Sucess..', productname);
          this
            .getRows()
            .then(s => {
              resolve(true)
            });
        }, e => {
          console.log('Inserted Error', e);
          resolve(false);
        })
    })
  }

  getRows() {
    return new Promise(res => {
      this.reviewArray = [];
      let query = "SELECT * FROM recentviews";
      this
        .db
        .executeSql(query, [], rs => {
          if (rs.rows.length > 0) {
            for (var i = 0; i < rs.rows.length; i++) {
              var item = rs
                .rows
                .item(i);
              this
                .reviewArray
                .push(item);
            }
          }
          res(true);
        }, (e) => {
          console.log('Sql Query Error', e);
        });
    })

  }
  //to delete any Item
  del(id) {
    return new Promise(resolve => {
      var query = "DELETE FROM recentviews WHERE id=?";
      this
        .db
        .executeSql(query, [id], (s) => {
          console.log('Delete Success...', s);
          this
            .getRows()
            .then(s => {
              resolve(true);
            });
        }, (err) => {
          console.log('Deleting Error', err);
        });
    })

  }
  delAll(){
    return new Promise(resolve => {
      // this.db.delete("favourites",null,null);
      var query = "DELETE FROM recentviews";
      this
        .db
        .executeSql(query,  (s) => {
          console.log('Delete Success...', s);
          this.db.executeSql("vacuum",()=>{
            resolve(true);
          });
        }, (err) => {
          console.log('Deleting Error', err);
        });
    })
  }
}
