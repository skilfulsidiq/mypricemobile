// import { IntroPage } from './../pages/intro/intro';
import { Component, ViewChild } from '@angular/core';
import {AlertController, Nav, Platform, LoadingController, Loading, ToastController, App} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
// import {LoginPage} from "../pages/login/login";
import {Storage} from "@ionic/storage";
import { Events } from 'ionic-angular';
import {ShareDataProvider} from "../providers/share-data/share-data";
import {AuthProvider} from "../providers/auth/auth";
import { OneSignal } from '@ionic-native/onesignal';
import {Badge} from "@ionic-native/badge";
import { PushProvider } from '../providers/push/push';
import {NetworkProvider} from "../providers/network/network";
import {UserProvider} from "../providers/user/user";
import {Network} from "@ionic-native/network";
// import { Subject } from 'rxjs/Subject';
// import { tap } from 'rxjs/operators';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:string;
  user;
  name;
  email;


  pages: Array<{title: string,icon:string, component: string}>;
  loading:Loading;

  constructor(public platform: Platform, public statusBar: StatusBar,
    public splashScreen: SplashScreen, public loadingCtrl:LoadingController,
    public alertCtrl:AlertController,public storage:Storage,public app:App,public network:Network,
              public event:Events, public push: Push,public badge: Badge,public pushpro:PushProvider,public networkPro:NetworkProvider,
              public sharedData:ShareDataProvider,public auth:AuthProvider,
              public toastCtrl: ToastController,public one:OneSignal) {

        this.storage.get('introShown').then((result) => {
          if(result){
              this.storage.get("hasLoggedIn").
                then((haslog) => {
                  // this.userSql.getUser();
                  if (haslog) {
                    this.rootPage = "LoginPage";
                  } else {
                    this.rootPage = "TabsPage"
                  }

                });

              //  }else{

                //  this.rootPage = "TabsPage";
              //  }

          } else{
            this.rootPage = "IntroPage";
            this.storage.set('introShown', true);

          }
          this.initializeApp();

          // this.loading.dismissAll();
        });

    // used for an example of ngFor and navigation
    this.pages = [
      {title:'My Profile',icon:"ios-person-outline",component:'ProfilePage'},
      {title:'My Favourites',icon:"ios-heart-outline",component:'FavouritePage'},
      // {title:'Home',icon:"ios-home-outline",component:'TabsPage'},
      // {title:'Search',icon:"ios-search-outline",component:'SearchPage'},
      {title:'My Shopping List',icon:"ios-list-outline",component:'ListPage'},
      {title:'My Recent Views',icon:"ios-eye-outline",component:'RecentPage'},
      // { title: 'Update Password', icon:"ios-lock-outline", component: 'ChangepasswordPage' },
      { title: 'Logout', icon:"ios-log-out-outline", component: null },

    ];
  }

  initializeApp() {
    // this.presentLoading();
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      },100);
      //check for network status
      // this.networkPro.initializeNetworkEvents();
      // // Offline event
      // this.event.subscribe('network:offline', () => {
      //   this.networkPro.presentToast('network:offline ==> '+this.network.type);
      //   console.log("u r offline");
      // });
      //
      // // Online event
      // this.event.subscribe('network:online', () => {
      //   this.networkPro.presentToast('network:online ==> '+this.network.type);
      //   console.log("u r online");
      // });
        this.networkPro.setSubscriptions();

        //push notification
        this.newNotification();

     //  let notificationOpenedCallback = (jsonData) => {
     //    this.nav.push("PushPage", { message: jsonData.notification.payload });
     //    console.log(jsonData.notification.payload.body);
     // };
     //  let notificatioReceived = (ok)=>{
     //      this.sharedData.saveNotice();
     //  }
     // if (window['plugins']) {
     //     window['plugins'].OneSignal.startInit("56f906ac-61de-4887-a64b-f5cbb2ead82a","983621471933").handleNotificationReceived(notificatioReceived)
     //          .handleNotificationOpened(notificationOpenedCallback)
     //          .endInit();
     // }
      //get user email

      this.exitApp();

    });


  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.component) {
      this.nav.setRoot(page.component);
    } else {
      // Since the component is null, this is the logout option
      // ...
          this.presentConfirm();
      // logout logic
      // ...

      // redirect to home

    }
    // this.nav.setRoot(page.component);
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Logging Out',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
              this.sharedData.logout();
            this.nav.setRoot("LoginPage");
            // this.platform.exitApp();
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  presentLoading() {

    this.loading = this.loadingCtrl.create({
      content: "Welcoming..."
    });

    this.loading.present();

  }
  showLoading() {
    if(!this.loading){
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration:1000
      });
      this.loading.present();
    }

  }
  dismissLoading(){
    if(this.loading){
      this.loading.dismiss();
      this.loading = null;
    }

  }

  showError(text) {
    this.dismissLoading();

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  initPushNotification() {
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {senderID: '983621471933'},
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);
    pushObject.on('notification').subscribe((data: any) => {
      console.log('message -> ' + data.message);
      //if user using app and push notification comes
      if (data.additionalData.foreground) {
        // if application open, show popup
        let confirmAlert = this.alertCtrl.create({
          title: 'New Notification',
          message: data.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              //TODO: Your logic here
              this.nav.push("PushPage", { message: data.message });
            }
          }]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        this.nav.push("PushPage", { message: data.message });
        console.log('Push notification clicked');
      }
    });
    pushObject.on('registration').subscribe((data: any) => {
      console.log('device token -> ' + data.registrationId);
      // TODO - send device token to server
    });
    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));


  }


  exitApp(){
    this.platform.registerBackButtonAction(() => {
      // const overlayView = this.app._appRoot._overlayPortal._views[0];
      // if (overlayView && overlayView.dismiss) {
      //   overlayView.dismiss();
      //   return;
      // }
      // console.log(this.app._appRoot._overlayPortal._views[0]);
      // if (this.nav.canGoBack()) {
      //     this.nav.pop();
      // }
      // else {
        let view = this.nav.getActive().id;
        console.log(view);
        if (view == "HomePage" || view == "LoginPage") {
          let alert = this.alertCtrl.create({
            title: 'Existing',
            message: 'Do you want exit?',
            buttons: [
              {
                text: 'No',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'Yes',
                handler: () => {

                  this.platform.exitApp();
                  console.log('Buy clicked');
                }
              }
            ]
          });
          alert.present();

        }

// }
    });
  }

  newNotification(){
    this.one.startInit("56f906ac-61de-4887-a64b-f5cbb2ead82a","983621471933");
    this.one.inFocusDisplaying(this.one.OSInFocusDisplayOption.InAppAlert);
    // InAppAlert
    this.one.handleNotificationReceived().subscribe((data: any) => {
      console.log(data.notification.payload.badge);
        this.pushpro.setBadges(1,data.notification.payload);
        this.pushpro.increaseBadges(1);

    //   const attempt = data.payload.additionalData.attempt;
    //   if (data.isAppInFocus) {
    //     // this.presentModalDefense(attempt);
    //     this.one.cancelNotification(data.androidNotificationId);
    //   } else {
    //     // this.badge.increase(1).then(data => {
    //     //   console.log(data);
    //     // });
    //   }
    //
    });
    this.one.handleNotificationOpened().subscribe((data: any) => {
        this.pushpro.clearAll();
        // this.pushpro.pushdata = '';
      console.log(data.notification.payload);
      // if(!data.isAppInFocus){
        this.nav.push("PushPage", { message: data.notification.payload });
        // this.pushpro.showbadge(false);
      // }else{
      //   // this.pushpro.captureNotice(data.notification.payload,data.notification.payload.badge);
      // }

      // const attempt = data.notification.payload.additionalData.attempt;
      // if (!data.isAppInFocus) {
      //   this.attemptPr.howLongTimeAgo(attempt.id).subscribe((timeago: any) => {
      //     let config = this.auth.getConfig();
      //     if (timeago.seconds >= config["SPEED_TO_ROB"].value) {
      //       this.presentModalUnsuccessfulDefense(attempt);
      //     } else {
      //       this.presentModalDefense(attempt, timeago.seconds);
      //     }
      //   }, err => {
      //
      //   });
      // }
    });
    this.one.endInit();
  }
}
