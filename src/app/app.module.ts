import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import {Storage} from "@ionic/storage";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
// import {AppProvider} from './ap';
import {HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Firebase } from '@ionic-native/firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Push } from '@ionic-native/push';
import {BrowserTab} from '@ionic-native/browser-tab';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import { ShareDataProvider } from '../providers/share-data/share-data';
import {LaunchNavigator} from "@ionic-native/launch-navigator";
import { SQLite } from '@ionic-native/sqlite';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { SqliteProvider } from '../providers/sqlite/sqlite';
import { ChecklistProvider } from '../providers/checklist/checklist';
import { Diagnostic } from '@ionic-native/diagnostic';
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";
import { OneSignal } from '@ionic-native/onesignal';
import { PushProvider } from '../providers/push/push';
import {Badge} from "@ionic-native/badge";
import { FavouriteProvider } from '../providers/favourite/favourite';
import { RecentviewProvider } from '../providers/recentview/recentview';
import { NetworkProvider } from '../providers/network/network';
import { UserProvider } from '../providers/user/user';
import {HttpModule} from "@angular/http";
import {Network} from "@ionic-native/network";

var config = {
  apiKey: "AIzaSyBgsft3bdvMa5Ejj3m0CueeYUkIFMdEzc8",
  authDomain: "my-price-d4e43.firebaseapp.com",
  databaseURL: "https://my-price-d4e43.firebaseio.com",
  projectId: "my-price-d4e43",
  storageBucket: "my-price-d4e43.appspot.com",
  messagingSenderId: "983621471933"
};
// let appProviders = AppProvider.getProviders();
@NgModule({
  declarations: [
    MyApp,
    // MapPage
    // HomePage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
    ,HttpModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(config),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // MapPage
    // HomePage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Storage,
    Geolocation,
    Network,
    BrowserTab,
    InAppBrowser,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,HttpClientModule,Push,
    ShareDataProvider,LaunchNavigator,SQLite,
    SqliteProvider,
    ChecklistProvider,
    Facebook,
    GooglePlus,
    Firebase,
    OneSignal,
    Diagnostic,
    PushProvider,Badge,
    FavouriteProvider,
    RecentviewProvider,
    NetworkProvider,
    UserProvider
  ]
})
export class AppModule {}
